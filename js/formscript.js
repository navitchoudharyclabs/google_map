/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

            var geocoder;
            var map;
            var marker;
            var markers = [];
            var input = document.getElementById('location');
            var searchform = document.getElementById('map-form');
            var place;
            var autocomplete = new google.maps.places.Autocomplete(input);
            //Add listener to detect autocomplete selection
            google.maps.event.addListener(autocomplete, 'place_changed', function () {
                place = autocomplete.getPlace();
            });

            //Add listener to search
            searchform.addEventListener("submit", function () {
                var newlatlong = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
                map.setCenter(newlatlong);
                marker.setPosition(newlatlong);
                map.setZoom(12);
                document.getElementById("lat").value = place.geometry.location.lat();
                document.getElementById("lng").value = place.geometry.location.lng();
                clearmarkers();
                markers.push(marker);
            });

            //Reset the inpout box on click
            input.addEventListener('click', function () {
                input.value = "";
            });
            var myCenter = new google.maps.LatLng(21, 78);
            function initialize() {
                geocoder = new google.maps.Geocoder();

                var mapProp = {
                    center: myCenter,
                    zoom: 5,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                };

                map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
                google.maps.event.addListener(map, 'click', function (event) {
                    placeMarker(event.latLng);
                });
                marker = new google.maps.Marker({
                    position: myCenter,
                    map: map,
                    title: 'Main map'

                });
            }

            google.maps.event.addDomListener(window, 'load', initialize);

            function placeMarker(l) {
                var marker = new google.maps.Marker({
                    position: l,
                    map: map
                });

                document.getElementById("lat").value = l.lat();
                document.getElementById("lng").value = l.lng();
                var infowindow = new google.maps.InfoWindow({
                    content: 'Latitude: ' + l.lat() + '<br>Longitude: ' + l.lng()
                });
                infowindow.open(map, marker);

                var geocoder;
                geocoder = new google.maps.Geocoder();
                var lat = parseFloat(l.lat());
                var lng = parseFloat(l.lng());
                var latlng = new google.maps.LatLng(lat, lng);
                geocoder.geocode({'latLng': latlng}, function (results, status) {
                    if (status === google.maps.GeocoderStatus.OK) {
                        var r = results[1].formatted_address;
                        document.getElementById("location").value = r;
                    }
                    else
                    {
                        alert('No Location Found');
                    }


                });

                clearmarkers();
                markers.push(marker);

            }

            function clearmarkers() {
                for (var i = 0; i < markers.length; i++)
                {
                    markers[i].setMap(null);
                }
            }





